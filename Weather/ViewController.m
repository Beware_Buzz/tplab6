//
//  ViewController.m
//  Weather
//
//  Created by Admin on 07.05.16.
//  Copyright © 2016 SmallTownGayBar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *indicator;
@property (weak, nonatomic) IBOutlet UITextField *country;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonOk:(id)sender {
    NSString *town = [[self country] text];
    NSString *stringUrl = [NSString stringWithFormat:@"%@%@%@", @"https://query.yahooapis.com/v1/public/yql?q=select%20item%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22",town, @"%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys"];
    
    NSURL *url = [[NSURL alloc] initWithString: stringUrl];
    
    NSData *contents = [[NSData alloc] initWithContentsOfURL:url];
    
    NSDictionary *forcasting = [NSJSONSerialization JSONObjectWithData:contents options:NSJSONReadingMutableContainers error:nil];
    
    NSString *weather = [[[[[[forcasting objectForKey: @"query" ] objectForKey: @"results"] objectForKey: @"channel"] objectForKey: @"item"] objectForKey: @"condition"] objectForKey: @"temp"];
    
    NSInteger farenheit = [[weather stringByReplacingOccurrencesOfString:@" " withString:@""] intValue];
    NSInteger celsium = 5 * (farenheit - 32) / 9;
    if (celsium > 0 ) {
        [[self indicator] setTextColor:[UIColor redColor]];
    } else [[self indicator] setTextColor:[UIColor blueColor]];
    
    weather = [NSString stringWithFormat: @"%d C", celsium];
    [[self indicator] setText: weather];
    
    
    
}

@end
